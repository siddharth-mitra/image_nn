FROM anibali/pytorch:cuda-10.0
WORKDIR /test/
COPY ./requirements.txt .
RUN pip install -r requirements.txt
EXPOSE 8080

