import torch
import torchvision
import torch.nn as nn
import torchvision.datasets as dsets
import torchvision.transforms as transforms
from torch.autograd import Variable
#hyper variables
num_classes = 10
num_epochs = 10
batch_size = 100
learning_rate = 0.001
t = transforms.Compose([transforms.ToTensor()])
data = dsets.MNIST('mnist_data',transform = t, download = True)
data_loader = torch.utils.data.DataLoader(dataset=data, batch_size=batch_size, shuffle=True)


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()  # Inherited from the parent class nn.Module
        self.linear1 = nn.Linear(28 * 28, 100)  # 1st Full-Connected Layer: 784 (input data) -> 500 (hidden node)
        self.relu = nn.ReLU()  # Non-Linear ReLU Layer: max(0,x)
        self.linear2 = nn.Linear(100, 50)  # 2nd Full-Connected Layer: 500 (hidden node) -> 10 (output class)
        self.final_layer = nn.Linear(50, num_classes)

    def forward(self, x):  # Forward pass: stacking each layer together
        x = x.view(-1, 28 * 28)
        x = self.relu(self.linear1(x))
        x = self.relu(self.linear2(x))
        x = self.final_layer(x)
        return x
#change
def train_model_A():
    net = Net()
    net.cuda()
    criterion = nn.CrossEntropyLoss()
    params = net.parameters()
    optimizer = torch.optim.Adam(params, lr=learning_rate)
    for epoch in range(num_epochs):
        for i, (images, labels) in enumerate(data_loader):
            images = Variable(images)
            labels = Variable(labels)
            images = images.cuda()
            labels = labels.cuda()
            output = net(images)
            net.zero_grad()
            loss = criterion(output, labels)
            loss.backward()
            optimizer.step()
    return "Training complete"

def main():
    net = Net()
    net.cuda()
    criterion = nn.CrossEntropyLoss()
    params = net.parameters()
    optimizer = torch.optim.Adam(params, lr=learning_rate)
    for epoch in range(num_epochs):
        for i, (images, labels) in enumerate(data_loader):
            images = Variable(images)
            labels = Variable(labels)
            images = images.cuda()
            labels = labels.cuda()
            output = net(images)
            net.zero_grad()
            loss = criterion(output, labels)
            loss.backward()
            optimizer.step()

if __name__ == '__main__':
    main()
